import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_crud/model/Customer.dart';
import 'package:simple_crud/model/User.dart';

class ApiService {
  SharedPreferences preferences;
  String accessToken;
  Client client = Client();

  final String baseUrl = "http://128.199.119.54/api";

  Future<User> getUser() async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};
    final response = await client.get("$baseUrl/user", headers: headers);
    if (response.statusCode == 200) {
      return usersFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<Customer>> getCustomers() async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};
    final response = await client.get("$baseUrl/customers", headers: headers);
    if (response.statusCode == 200) {
      return customersFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<Customer>> getFavorite() async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};
    final response = await client.get("$baseUrl/favorite", headers: headers);
    if (response.statusCode == 200) {
      return customersFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> addToFavorite(int id) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};

    final response = await client.post("$baseUrl/favorite/add",
        headers: headers, body: {"customer_id": id.toString()});

    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> deleteFromFavorite(int id) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};

    final response = await client.post("$baseUrl/favorite/delete",
        headers: headers, body: {"customer_id": id.toString()});

    return json.decode(response.body);
  }

  Future<Customer> showCustomers(int id) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};

    final response =
        await client.get("$baseUrl/customers/$id", headers: headers);

    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      return Customer.fromJson(data["data"]);
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> addCustomer(Map<String, dynamic> data) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};

    final response = await client.post("$baseUrl/customers/add",
        headers: headers, body: data);

    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> updateCustomer(Map<String, dynamic> data) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};
    final response = await client.post("$baseUrl/customers/update",
        headers: headers, body: data);

    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> deleteCustomer(int id) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};

    final response = await client.post("$baseUrl/customers/delete",
        headers: headers, body: {"customer_id": id.toString()});

    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> login(String email, String password) async {
    final response = await client.post("$baseUrl/login",
        body: {"email": email.trim(), "password": password});

    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> register(String name, String email,
      String password, String confirmPassword) async {
    final response = await client.post("$baseUrl/register", body: {
      "name": name.trim(),
      "email": email.trim(),
      "password": password,
      "confirm_password": confirmPassword
    });

    return json.decode(response.body);
  }

  Future<Map<String, dynamic>> updateProfile(Map<String, dynamic> data) async {
    preferences = await SharedPreferences.getInstance();

    accessToken = preferences.getString("access_token");

    var headers = {"Authorization": "Bearer $accessToken"};
    final response = await client.post("$baseUrl/update_profile",
        headers: headers, body: data);
    return json.decode(response.body);
  }
}
