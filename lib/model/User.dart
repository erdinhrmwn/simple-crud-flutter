import 'dart:convert';

class User {
  String name;
  String email;
  String address;
  int age;

  User({this.name, this.email, this.address, this.age});

  factory User.fromJson(Map<String, dynamic> map) {
    return User(
      name: map["name"],
      email: map["email"],
      address: map["address"],
      age: map["age"],
    );
  }

  Map<String, dynamic> toJson() {
    return {"name": name, "email": email, "address": address, "age": age};
  }
}

User usersFromJson(String jsonData) {
  final data = json.decode(jsonData);
  final user = data["data"];
  return User.fromJson(user);
}

String userToJson(User data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
