import 'dart:convert';

class Customer {
  int id;
  String name;
  String email;
  String address;
  int age;
  int isFavorite;

  Customer({
    this.id = 0,
    this.name,
    this.email,
    this.address,
    this.age,
    this.isFavorite,
  });

  factory Customer.fromJson(Map<String, dynamic> map) {
    return Customer(
      id: map["id"],
      name: map["name"],
      email: map["email"],
      address: map["address"],
      age: map["age"],
      isFavorite: map["is_favorite"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "email": email,
      "address": address,
      "isFavorite": isFavorite,
      "age": age
    };
  }

  @override
  String toString() {
    return 'Customer{id: $id, name: $name, email: $email, address: $address, isFavorite: $isFavorite, age: $age}';
  }
}

List<Customer> customersFromJson(String jsonData) {
  final data = json.decode(jsonData);
  final customers = data["data"];
  return List<Customer>.from(customers.map(
    (item) => Customer.fromJson(item),
  )).reversed.toList();
}

String customerToJson(Customer data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
