import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_crud/api/ApiService.dart';
import 'package:simple_crud/model/Customer.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ApiService apiService = ApiService();
  StreamController<List<Customer>> _customersStream;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _customersStream = StreamController<List<Customer>>();

    _loadCustomers();
  }

  void _loadCustomers() async =>
      apiService.getCustomers().then((v) => _customersStream.add(v));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Homepage"),
        leading: IconButton(
          icon: Icon(FontAwesomeIcons.plus),
          onPressed: () async => await Navigator.pushNamed(context, '/add')
              .then((c) => _loadCustomers()),
        ),
      ),
      body: Container(
        child: _buildListView(false),
      ),
    );
  }

  Widget _buildListView(bool justFavorite) {
    return StreamBuilder<List<Customer>>(
      stream: _customersStream.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var data = snapshot.data;
          if (data.length > 0) {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: ListView.builder(
                itemBuilder: (context, index) {
                  var customer = data.elementAt(index);
                  return InkWell(
                    child: _customerDetailView(customer),
                    onTap: () => Navigator.pushNamed(
                      context,
                      '/show',
                      arguments: customer,
                    ).then((context) => _loadCustomers()),
                  );
                },
                itemCount: data.length,
              ),
            );
          } else {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Card(
                child: Container(
                  height: 150,
                  color: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "You don't have any customer.",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        Text(
                          "you can add your first customer.",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              MaterialButton(
                                color: Colors.red[400],
                                onPressed: () =>
                                    Navigator.pushNamed(context, '/add')
                                        .then((context) => _loadCustomers()),
                                child: Text(
                                  "Add",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        }

        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Widget _customerDetailView(Customer customer) {
    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: Card(
        child: Container(
          height: 150,
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  customer.name,
                  style: Theme.of(context).textTheme.headline6,
                  overflow: TextOverflow.ellipsis,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      customer.email,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      "${customer.age.toString()} y.o",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
                Container(
                  width: 300,
                  child: Text(
                    customer.address,
                    style: Theme.of(context).textTheme.bodyText2,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          customer.isFavorite == 1
                              ? FontAwesomeIcons.solidHeart
                              : FontAwesomeIcons.heart,
                          color: customer.isFavorite == 1 ? Colors.red : null,
                        ),
                        onPressed: () {
                          if (customer.isFavorite == 1)
                            apiService
                                .deleteFromFavorite(customer.id)
                                .then((c) => _loadCustomers());
                          else
                            apiService
                                .addToFavorite(customer.id)
                                .then((c) => _loadCustomers());
                        },
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 8),
                            child: MaterialButton(
                              color: Colors.red[400],
                              onPressed: () => _showDialog(customer.id),
                              child: Text(
                                "Delete",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          MaterialButton(
                            color: Colors.blue[400],
                            onPressed: () async => await Navigator.pushNamed(
                              context,
                              '/edit',
                              arguments: customer,
                            ).then((v) => _loadCustomers()),
                            child: Text(
                              "Edit",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showDialog(int id) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Are you sure?"),
          content: Text("This action cannot be undone."),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Delete"),
              color: Colors.red[400],
              onPressed: () => apiService.deleteCustomer(id).then((v) {
                Navigator.of(context).pop();
                _loadCustomers();
                Fluttertoast.showToast(msg: "Customer successfully deleted.");
              }),
            ),
          ],
        );
      },
    );
  }
}
