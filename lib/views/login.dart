import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_crud/api/ApiService.dart';
import 'package:simple_crud/views/register.dart';

class LoginView extends StatefulWidget {
  LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _formKey = GlobalKey<FormState>();

  SharedPreferences preferences;
  bool isLoading = false, isLoggedIn = false;

  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    isSignedIn();
  }

  void isSignedIn() async {
    setState(() {
      isLoading = true;
    });

    preferences = await SharedPreferences.getInstance();
    isLoggedIn = preferences.getString("access_token") != null;
    if (isLoggedIn) {
      Navigator.pushReplacementNamed(context, '/');
    }

    setState(() {
      isLoading = false;
    });
  }

  Future<void> signIn() async {
    var fromState = _formKey.currentState;
    if (fromState.validate()) {
      ApiService()
          .login(_emailTextController.text, _passwordTextController.text)
          .then((value) async {
        if (value["success"]) {
          Fluttertoast.showToast(msg: "Login Successfully.");
          var data = value["data"];
          await preferences.setString("name", data["name"]);
          await preferences.setString("email", data["email"]);
          await preferences.setString("access_token", data["access_token"]);
          isSignedIn();
        } else {
          Fluttertoast.showToast(msg: value["error_description"]);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "LOGIN",
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      icon: Icon(Icons.alternate_email),
                      hintText: "Email",
                    ),
                    controller: _emailTextController,
                    validator: (value) {
                      if (value.isNotEmpty) {
                        Pattern pattern =
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                        RegExp regex = new RegExp(pattern);
                        return (regex.hasMatch(value.trim()))
                            ? null
                            : "Please check your email address.";
                      } else {
                        return "Please check your email address.";
                      }
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      hintText: "Password",
                    ),
                    obscureText: true,
                    controller: _passwordTextController,
                    validator: (value) {
                      return value.isNotEmpty
                          ? null
                          : "Please check your password.";
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.red[400],
                  child: MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    onPressed: signIn,
                    child: Text(
                      "SIGN IN",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Doesn't have an account? Register ",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontSize: 14),
                  ),
                  InkWell(
                    child: Text(
                      "here",
                      style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.w400,
                          fontSize: 14),
                    ),
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, '/register'),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
