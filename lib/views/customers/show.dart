import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_crud/api/ApiService.dart';
import 'package:simple_crud/model/Customer.dart';

class ShowCustomerView extends StatefulWidget {
  ShowCustomerView({Key key, this.customer}) : super(key: key);
  final Customer customer;

  @override
  ShowCustomerViewState createState() => ShowCustomerViewState();
}

class ShowCustomerViewState extends State<ShowCustomerView> {
  ApiService apiService = ApiService();
  StreamController<Customer> _customerStream;
  Customer customer;

  @override
  void initState() {
    super.initState();

    _customerStream = StreamController<Customer>();
    _loadCustomers();
  }

  void _loadCustomers() async {
    apiService
        .showCustomers(widget.customer.id)
        .then((v) => _customerStream.add(v));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details"),
      ),
      body: StreamBuilder<Customer>(
          stream: _customerStream.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              customer = snapshot.data;
              return ListView(
                children: <Widget>[
                  ListTile(
                    title: Text("Name"),
                    subtitle: Text(customer.name),
                    leading: Icon(
                      FontAwesomeIcons.solidUser,
                      size: 35,
                    ),
                  ),
                  ListTile(
                    title: Text("Email"),
                    subtitle: Text(customer.email),
                    leading: Icon(
                      FontAwesomeIcons.at,
                      size: 35,
                    ),
                  ),
                  ListTile(
                    title: Text("Age"),
                    subtitle: Text(customer.age.toString() + " y.o"),
                    leading: Icon(
                      FontAwesomeIcons.sortNumericUp,
                      size: 35,
                    ),
                  ),
                  ListTile(
                    title: Text("Address"),
                    subtitle: Text(customer.address),
                    leading: Icon(
                      FontAwesomeIcons.home,
                      size: 35,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 8),
                        child: MaterialButton(
                          color: Colors.red[400],
                          onPressed: () => _showDialog(customer.id),
                          child: Text(
                            "Delete",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      MaterialButton(
                        color: Colors.blue[400],
                        onPressed: () async => await Navigator.pushNamed(
                          context,
                          '/edit',
                          arguments: customer,
                        ).then((v) => _loadCustomers()),
                        child: Text(
                          "Edit",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }

  void _showDialog(int id) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Are you sure?"),
          content: Text("This action cannot be undone."),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Delete"),
              color: Colors.red[400],
              onPressed: () => apiService.deleteCustomer(id).then((v) {
                Navigator.of(context).popUntil(ModalRoute.withName('/'));
                Fluttertoast.showToast(msg: "Customer successfully deleted.");
              }),
            ),
          ],
        );
      },
    );
  }
}
