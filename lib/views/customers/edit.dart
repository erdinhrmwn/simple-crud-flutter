import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_crud/api/ApiService.dart';
import 'package:simple_crud/model/Customer.dart';

class EditCustomerView extends StatefulWidget {
  EditCustomerView({Key key, this.customer}) : super(key: key);
  final Customer customer;

  @override
  _EditCustomerViewState createState() => _EditCustomerViewState();
}

class _EditCustomerViewState extends State<EditCustomerView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _nameTextController = TextEditingController(),
      _emailTextController = TextEditingController(),
      _addressTextController = TextEditingController(),
      _ageTextController = TextEditingController();

  @override
  void initState() {
    super.initState();

    setState(() {
      _nameTextController.text = widget.customer.name;
      _emailTextController.text = widget.customer.email;
      _addressTextController.text = widget.customer.address;
      _ageTextController.text = widget.customer.age.toString();
    });
  }

  void updateCustomer() async {
    if (_formKey.currentState.validate()) {
      final data = {
        "customer_id": widget.customer.id.toString(),
        "name": _nameTextController.text,
        "email": _emailTextController.text,
        "age": _ageTextController.text,
        "address": _addressTextController.text
      };

      ApiService().updateCustomer(data).then((value) async {
        if (value["success"]) {
          Fluttertoast.showToast(msg: value["message"]);
          Navigator.of(context).pop();
        } else {
          Fluttertoast.showToast(msg: value["error_description"]);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Customer"),
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: InputDecoration(
                    icon: Icon(FontAwesomeIcons.solidUser),
                    hintText: "Please input your name.",
                    labelText: "Name",
                  ),
                  autocorrect: true,
                  controller: _nameTextController,
                  validator: (value) {
                    return (value.isNotEmpty)
                        ? null
                        : "Please check your name.";
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    icon: Icon(FontAwesomeIcons.at),
                    hintText: "Please input your email.",
                    labelText: "Email",
                  ),
                  controller: _emailTextController,
                  validator: (value) {
                    if (value.isNotEmpty) {
                      Pattern pattern =
                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                      RegExp regex = new RegExp(pattern);
                      return (regex.hasMatch(value.trim()))
                          ? null
                          : "Please check your email address.";
                    } else {
                      return "Please check your email address.";
                    }
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    icon: Icon(FontAwesomeIcons.sortNumericUp),
                    hintText: "Please input your age.",
                    labelText: "Age (17-30)",
                    suffixText: "y.o",
                  ),
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ], // Only numbers can be entered
                  controller: _ageTextController,
                  validator: (value) {
                    var age = int.parse(value);
                    if (age < 17 || age > 30) {
                      return "Age must be between 17 - 30 years.";
                    }

                    return (value.isNotEmpty) ? null : "Please check your age.";
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: TextFormField(
                  minLines: 1,
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(
                    alignLabelWithHint: true,
                    icon: Icon(FontAwesomeIcons.home),
                    hintText: "Please input your address.",
                    labelText: "Address",
                  ),
                  controller: _addressTextController,
                  validator: (value) {
                    return (value.isNotEmpty)
                        ? null
                        : "Please check your address.";
                  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.red[400],
                  child: MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    onPressed: updateCustomer,
                    child: Text(
                      "submit".toUpperCase(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
