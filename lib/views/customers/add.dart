import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:simple_crud/api/ApiService.dart';

class AddCustomerView extends StatefulWidget {
  AddCustomerView({Key key}) : super(key: key);

  @override
  _AddCustomerViewState createState() => _AddCustomerViewState();
}

class _AddCustomerViewState extends State<AddCustomerView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _nameTextController = TextEditingController(),
      _emailTextController = TextEditingController(),
      _addressTextController = TextEditingController(),
      _ageTextController = TextEditingController();

  void addCustomer() async {
    if (_formKey.currentState.validate()) {
      final data = {
        "name": _nameTextController.text,
        "email": _emailTextController.text,
        "age": _ageTextController.text,
        "address": _addressTextController.text
      };

      ApiService().addCustomer(data).then((value) async {
        if (value["success"]) {
          Fluttertoast.showToast(msg: value["message"]);
          Navigator.of(context).pop();
        } else {
          Fluttertoast.showToast(msg: value["error_description"]);
        }
      });
    }
  }

  void _generateData() {
    setState(() {
      _nameTextController.text = faker.person.name();
      _emailTextController.text = faker.internet.email();
      _addressTextController.text =
          "${faker.address.streetAddress()}, ${faker.address.city()}, ${faker.address.continent()}, ${faker.address.country()}";
      _ageTextController.text =
          faker.randomGenerator.integer(30, min: 17).toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Customer"),
        actions: <Widget>[
          FlatButton(
            child: Text(
              "Generate",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: _generateData,
          )
        ],
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      icon: Icon(FontAwesomeIcons.solidUser),
                      hintText: "Please input your name.",
                      labelText: "Name",
                    ),
                    autocorrect: true,
                    controller: _nameTextController,
                    validator: (value) {
                      return (value.isNotEmpty)
                          ? null
                          : "Please check your name.";
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      icon: Icon(FontAwesomeIcons.at),
                      hintText: "Please input your email.",
                      labelText: "Email",
                    ),
                    controller: _emailTextController,
                    validator: (value) {
                      if (value.isNotEmpty) {
                        Pattern pattern =
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                        RegExp regex = new RegExp(pattern);
                        return (regex.hasMatch(value.trim()))
                            ? null
                            : "Please check your email address.";
                      } else {
                        return "Please check your email address.";
                      }
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      icon: Icon(FontAwesomeIcons.sortNumericUp),
                      hintText: "Please input your age.",
                      labelText: "Age (17-30)",
                      suffixText: "y.o",
                    ),
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly
                    ], // Only numbers can be entered
                    controller: _ageTextController,
                    validator: (value) {
                      if (value.isNotEmpty) {
                        var age = int.parse(value);
                        if (age < 17 || age > 30) {
                          return "Age must be between 17 - 30 years.";
                        } else {
                          return null;
                        }
                      } else {
                        return "Please check your age.";
                      }
                    },
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: TextFormField(
                    minLines: 1,
                    maxLines: null,
                    textAlignVertical: TextAlignVertical.top,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      alignLabelWithHint: true,
                      icon: Icon(FontAwesomeIcons.home),
                      hintText: "Please input your address.",
                      labelText: "Address",
                    ),
                    controller: _addressTextController,
                    validator: (value) {
                      return (value.isNotEmpty)
                          ? null
                          : "Please check your address.";
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.red[400],
                  child: MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    onPressed: addCustomer,
                    child: Text(
                      "Submit",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
