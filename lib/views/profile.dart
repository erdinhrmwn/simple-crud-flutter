import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_crud/api/ApiService.dart';
import 'package:simple_crud/model/User.dart';

class ProfileView extends StatefulWidget {
  ProfileView({Key key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  SharedPreferences preferences;
  StreamController<User> _userStream;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _userStream = StreamController<User>();

    _checkUser();
  }

  void _checkUser() async {
    preferences = await SharedPreferences.getInstance();

    ApiService().getUser().then((u) => _userStream.add(u));
  }

  void logout() async {
    await preferences.clear();

    Navigator.pushReplacementNamed(context, '/login');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.userEdit),
            onPressed: () => Navigator.of(context)
                .pushNamed('/edit_profile')
                .then((context) => _checkUser()),
          )
        ],
      ),
      body: StreamBuilder(
        stream: _userStream.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final User _user = snapshot.data;
            return Padding(
              padding: EdgeInsets.all(8.0),
              child: ListView(
                children: <Widget>[
                  Container(
                    width: 75,
                    height: 75,
                    child: Icon(
                      FontAwesomeIcons.solidUserCircle,
                      size: 75,
                    ),
                  ),
                  Divider(),
                  ListTile(
                    title: Text("Name"),
                    subtitle: Text(_user.name),
                  ),
                  Divider(),
                  ListTile(
                    title: Text("Email"),
                    subtitle: Text(_user.email),
                  ),
                  Divider(),
                  ListTile(
                    title: Text("Age"),
                    subtitle: Text(_user.age.toString()),
                  ),
                  Divider(),
                  ListTile(
                    title: Text("Address"),
                    subtitle: Text(_user.address),
                  ),
                  Divider(),
                  MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    child: Text(
                      "Logout",
                      style: TextStyle(color: Colors.red),
                    ),
                    onPressed: logout,
                  )
                ],
              ),
            );
          } else
            return Divider();
        },
      ),
    );
  }
}

class EditProfileView extends StatefulWidget {
  EditProfileView({Key key}) : super(key: key);

  @override
  _EditProfileViewState createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _ageTextController = TextEditingController();
  TextEditingController _addressTextController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
  }

  void getUser() => ApiService().getUser().then((u) => reloadFormWith(u));

  void reloadFormWith(User user) {
    _nameTextController.text = user.name;
    _emailTextController.text = user.email;
    _ageTextController.text = user.age.toString();
    _addressTextController.text = user.address;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
        actions: <Widget>[
          FlatButton(
            child: Text(
              "Reset",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: getUser,
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextFormField(
                      controller: _nameTextController,
                      textCapitalization: TextCapitalization.sentences,
                      decoration: InputDecoration(
                        icon: Icon(FontAwesomeIcons.solidUser),
                        labelText: "Name",
                        hintText: "Input your name.",
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextFormField(
                      controller: _emailTextController,
                      decoration: InputDecoration(
                        icon: Icon(FontAwesomeIcons.at),
                        labelText: "Email",
                        hintText: "Input your email address.",
                      ),
                      readOnly: true,
                      enabled: false,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: _ageTextController,
                      decoration: InputDecoration(
                        icon: Icon(FontAwesomeIcons.sortNumericUp),
                        labelText: "Age",
                        hintText: "Input your age.",
                      ),
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextFormField(
                      controller: _addressTextController,
                      minLines: 1,
                      maxLines: null,
                      keyboardType: TextInputType.multiline,
                      decoration: InputDecoration(
                        icon: Icon(FontAwesomeIcons.home),
                        hintText: "Please input your address.",
                        labelText: "Address",
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.red[400],
                      child: MaterialButton(
                        minWidth: MediaQuery.of(context).size.width,
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        onPressed: updateProfile,
                        child: Text(
                          "save".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void updateProfile() {
    final data = {
      "name": _nameTextController.text,
      "age": _ageTextController.text,
      "address": _addressTextController.text
    };

    ApiService().updateProfile(data).then((value) async {
      if (value["success"]) {
        Fluttertoast.showToast(msg: value["message"]);
        Navigator.of(context).pop();
      } else {
        Fluttertoast.showToast(msg: value["error_description"]);
      }
    });
  }
}
