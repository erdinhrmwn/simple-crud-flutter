import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_crud/api/ApiService.dart';
import 'package:simple_crud/views/customers/edit.dart';
import 'package:simple_crud/views/customers/show.dart';
import 'package:simple_crud/views/favorite.dart';
import 'package:simple_crud/views/home.dart';
import 'package:simple_crud/views/login.dart';
import 'package:simple_crud/views/customers/add.dart';
import 'package:simple_crud/views/profile.dart';
import 'package:simple_crud/views/register.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        primaryColor: Colors.red[400],
      ),
      routes: {
        '/login': (context) => LoginView(),
        '/register': (context) => RegisterView(),
        '/add': (context) => AddCustomerView(),
        '/show': (context) => ShowCustomerView(
            customer: ModalRoute.of(context).settings.arguments),
        '/edit': (context) => EditCustomerView(
            customer: ModalRoute.of(context).settings.arguments),
        '/edit_profile': (context) => EditProfileView(),
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  SharedPreferences preferences;
  ApiService apiService = ApiService();
  bool isLoggedIn = false;

  int currentIndex = 0;
  List<Widget> widgetList;

  @override
  void initState() {
    super.initState();
    widgetList = [
      HomePage(),
      FavoritePage(),
      ProfileView(),
    ];

    _checkUser();
  }

  void _checkUser() async {
    preferences = await SharedPreferences.getInstance();
    isLoggedIn = preferences.getString("access_token") != null;
    if (!isLoggedIn) {
      Navigator.pushReplacementNamed(context, '/login');
    }
  }

  void logout() async {
    await preferences.clear();

    setState(() {
      isLoggedIn = false;
    });

    _checkUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetList[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (value) => setState(() => currentIndex = value),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.solidHeart),
            title: Text("Favorite"),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.solidUser),
            title: Text("Profile"),
          ),
        ],
      ),
    );
  }
}
